package org.mazine.userProfileApi.infra;

import java.util.*;

public class UserProfileRepository {
    private static final Map<UUID, UserProfile> profiles = new HashMap<>();

    public void save(UserProfile profile) {
        profiles.put(profile.getId(), profile);
    }

    public List<UserProfile> findAll() {
        return new ArrayList<>(profiles.values());
    }

    public UserProfile find(UUID uuid) {
        return profiles.get(uuid);
    }

    public void delete(UUID uuid) {
        profiles.remove(uuid);
    }
}
