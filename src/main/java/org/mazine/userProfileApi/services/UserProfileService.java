package org.mazine.userProfileApi.services;

import org.mazine.userProfileApi.infra.UserProfile;
import org.mazine.userProfileApi.infra.UserProfileRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class UserProfileService {
    private UserProfileRepository userProfileRepository;

    public UserProfileService() {
        userProfileRepository = new UserProfileRepository();
    }

    public UserProfileDto createProfile(CreateUserProfileDto dto) {
        UserProfile userProfile = toUserProfile(dto);
        userProfileRepository.save(userProfile);

        return toDto(userProfile);
    }

    public UserProfilesDto getAllProfiles() {
        List<UserProfileDto> profiles = userProfileRepository.findAll()
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());

        UserProfilesDto profilesDto = new UserProfilesDto();
        profilesDto.profiles = profiles;

        return profilesDto;
    }

    public UserProfileDto getProfile(UUID userId) {
        UserProfile foundProfile = userProfileRepository.find(userId);

        return toDto(foundProfile);
    }

    public UserProfileDto updateProfile(UserProfileDto userProfileDto) {
        if (userProfileRepository.find(userProfileDto.id) == null) {
            throw new RuntimeException(String.format("Profile with id [%s} not found", userProfileDto.id));
        }

        UserProfile userProfile = toUserProfile(userProfileDto);
        userProfileRepository.save(userProfile);

        return userProfileDto;
    }

    public void deleteProfile(UUID userId) {
        userProfileRepository.delete(userId);
    }

    private UserProfile toUserProfile(CreateUserProfileDto dto) {
        return new UserProfile(dto.name, dto.email);
    }

    private UserProfile toUserProfile(UserProfileDto dto) {
        return new UserProfile(dto.id, dto.name, dto.email);
    }


    private UserProfileDto toDto(UserProfile userProfile) {
        UserProfileDto dto = new UserProfileDto();
        dto.id = userProfile.getId();
        dto.name = userProfile.getName();
        dto.email = userProfile.getEmail();

        return dto;
    }
}
