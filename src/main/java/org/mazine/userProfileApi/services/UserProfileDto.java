package org.mazine.userProfileApi.services;

import java.util.UUID;

public class UserProfileDto {
    public UUID id;
    public String name;
    public String email;
}
