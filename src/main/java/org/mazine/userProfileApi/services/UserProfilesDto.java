package org.mazine.userProfileApi.services;

import java.util.List;

public class UserProfilesDto {
    public List<UserProfileDto> profiles;
}
