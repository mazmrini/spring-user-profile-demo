package org.mazine.userProfileApi.api;

import org.mazine.userProfileApi.services.CreateUserProfileDto;
import org.mazine.userProfileApi.services.UserProfileDto;
import org.mazine.userProfileApi.services.UserProfileService;
import org.mazine.userProfileApi.services.UserProfilesDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.UUID;

@RestController
public class UserProfileController {
    private UserProfileService userProfileService;

    public UserProfileController() {
        userProfileService = new UserProfileService();
    }

    @PostMapping("/profiles")
    public UserProfileDto postProfile(@RequestBody CreateUserProfileDto dto) {
        return userProfileService.createProfile(dto);
    }

    @GetMapping("/profiles")
    public UserProfilesDto getProfiles() {
        return userProfileService.getAllProfiles();
    }

    @GetMapping("/profiles/{id}")
    public UserProfileDto getProfiles(@PathVariable("id") UUID id) {
        return userProfileService.getProfile(id);
    }

    @PutMapping("/profiles/{id}")
    public UserProfileDto putProfile(@PathVariable("id") UUID id,
                                     @RequestBody CreateUserProfileDto dto) {
        UserProfileDto userProfileDto = new UserProfileDto();
        userProfileDto.id = id;
        userProfileDto.name = dto.name;
        userProfileDto.email = dto.email;

        return userProfileService.updateProfile(userProfileDto);
    }

    @DeleteMapping("/profiles/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProfile(@PathVariable("id") UUID id) {
        userProfileService.deleteProfile(id);
    }
}
