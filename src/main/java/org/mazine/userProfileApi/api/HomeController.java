package org.mazine.userProfileApi.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/")
    public WelcomeDto getHome() {
        WelcomeDto welcomeDto = new WelcomeDto();
        welcomeDto.status = "green";
        welcomeDto.welcomeMessage = "Welcome to the api!";

        return welcomeDto;
    }
}
